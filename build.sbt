resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"
resolvers += "scala-tools.org" at "http://scala-tools.org/repo-releases"
resolvers += Resolver.url("bintray-sbt-plugins", url("http://dl.bintray.com/sbt/sbt-plugin-releases"))(Resolver.ivyStylePatterns)

name := "kafka-consumergroupchecker"
version := "1.0"
scalaVersion := "2.12.1"
retrieveManaged := true

libraryDependencies += "com.amazonaws" % "aws-lambda-java-core" % "1.1.0"
libraryDependencies += "com.amazonaws" % "aws-lambda-java-events" % "1.3.0"
libraryDependencies += "junit" % "junit" % "4.8.2" % "test"
//libraryDependencies += "org.specs" % "specs" % "1.2.5" % "test"
libraryDependencies += "org.apache.kafka" %% "kafka" % "0.10.2.0"
libraryDependencies += "org.easymock" % "easymock" % "3.4" % "test"
libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.1"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.1" % "test"
libraryDependencies += "org.apache.logging.log4j" % "log4j-core" % "2.9.0"

parallelExecution in Test := false

// for debugging sbt problems
logLevel := Level.Info

scalacOptions += "-deprecation"

javacOptions ++= Seq("-source", "1.8", "-target", "1.8", "-Xlint")


