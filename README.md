# kafka-consumergroupchecker
- Project to monitor Kafka ConsumerGroups Lagged/Missing offsets.

## Compile
```
$ git clone https://gitlab.com/gfleury/kafka-consumergroupchecker.git
$ cd kafka-consumergroupchecker
```
#### with maven
```
$ mvn install -B
```
#### with sbt (not proper tested use Maven)
```
$ sbt package
```
## Run
### Parameters
```
Use: cmd propertie.file
or   cmd bootstrap-server topics forever
or   create config.properties and run without args
propertie request keys: bootstrap-server, topics, run-forever
Examples: topics = t1,t2,t3 or t1 
          run-forever = true or false
```
- First parameter is the broker hostname
- Second parameter is the topic list to be monitored
- Third option is if the tool should loop forever or should just run once and exit.
### 1st - Run checking topics t1,t2 and t3 on kafka running locally (localhost)
```
$ java -jar target/kafka-consumergroupchecker-0.0.1-SNAPSHOT.jar localhost:9092 t1,t2,t3 true
```
### 2nd - Create config file
```
$ cat config.properties 
bootstrap-server=localhost:9092
topics=t1,t2,t3
run-forever=true
$ java -jar target/kafka-consumergroupchecker-0.0.1-SNAPSHOT.jar config.properties
```

## Expected output
[date/time] ERROR CG_Status - GroupID:ConsumerGroupName Lagged topic:topic partition:partitionID ActualOffset:Topic/Partition Offset LaggedBy:Offset Lagged (consumer-info)
### Example
```
[2017-09-19 19:27:26,881] INFO Note: This will only show information about consumers that use the Java consumer API (non-ZooKeeper-based consumers).
 (ConsumerGroupChecker:81)
[2017-09-19 19:29:42,692] ERROR Stable - GroupId:console-consumer-60987 Lagged topic:t2 partition:0 ActualOffset:520 LaggedBy:520 (consumer-1-2f193ef2-714c-4647-8b6f-de184e41b78d//192.168.1.171/consumer-1) (ConsumerGroupChecker:137)
[2017-09-19 19:29:44,703] ERROR Stable - GroupId:console-consumer-60987 Lagged topic:t2 partition:0 ActualOffset:520 LaggedBy:1300 (consumer-1-2f193ef2-714c-4647-8b6f-de184e41b78d//192.168.1.171/consumer-1) (ConsumerGroupChecker:137)
[2017-09-19 19:29:46,720] ERROR Stable - GroupId:console-consumer-60987 Lagged topic:t2 partition:0 ActualOffset:1820 LaggedBy:520 (consumer-1-2f193ef2-714c-4647-8b6f-de184e41b78d//192.168.1.171/consumer-1) (ConsumerGroupChecker:137)
[2017-09-19 19:29:48,733] ERROR Stable - GroupId:console-consumer-60987 Lagged topic:t2 partition:0 ActualOffset:1820 LaggedBy:1300 (consumer-1-2f193ef2-714c-4647-8b6f-de184e41b78d//192.168.1.171/consumer-1) (ConsumerGroupChecker:137)
```
