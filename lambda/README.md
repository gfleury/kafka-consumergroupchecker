# kafka-consumergroupchecker lambda wrapper
## Deploy with Cloudformation 
```
$ ./deploy_stack.sh s3_bucket
```
- S3 bucket name is necessary for creating the Cloudformation package to deploy the lambda function. 

## Deploy with shell scripts
```
$ ./create_iam.sh
(copy the new role ARN and user on the bellow cmd)
$ ./lambda.sh --profile AWS --role arn:aws:iam::000030303:role/kafka-consumergroupchecker --vpc SubnetIds=subnet-bf59dee7,SecurityGroupIds=sg-f7c82c8a --environment "Variables={bootstrap_server=172.31.12.116:9092,topics=t1,run_forever=false}"
```

Change the vars from subnet/securitygroup and Variables with ec2 kafka vars. 