aws lambda create-function \
--region us-west-2 \
--function-name kafka-consumergroupchecker \
--zip-file fileb://../target/kafka-consumergroupchecker-0.0.1-SNAPSHOT.jar \
--handler com.scout24.monitoring.kafka.ConsumerGroupChecker.Main::lambda \
--runtime java8 \
--timeout 300 \
--profile adminuser \
$@
