[ $# -lt 1 ] && exit 
aws cloudformation package --template-file cfn.yaml --s3-bucket $1 --output-template-file output_template.yaml || exit
aws cloudformation deploy --template output_template.yaml --stack-name kafkaConsumer$RANDOM --capabilities CAPABILITY_IAM || exit 

