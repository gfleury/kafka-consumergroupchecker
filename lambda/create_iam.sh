aws iam create-role --role-name kafka-consumergroupchecker --assume-role-policy-document file://lambda.json
aws iam attach-role-policy --role-name kafka-consumergroupchecker --policy-arn arn:aws:iam::aws:policy/service-role/AWSLambdaENIManagementAccess
aws iam attach-role-policy --role-name kafka-consumergroupchecker --policy-arn arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole
