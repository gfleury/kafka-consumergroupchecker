package com.scout24.monitoring.test.utils

import java.io.IOException
import java.util.Properties
import kafka.server.KafkaConfig
import kafka.server.KafkaServerStartable

import java.io.FileNotFoundException
import java.io.IOException
import java.util.Properties

import org.apache.zookeeper.server.ServerConfig
import org.apache.zookeeper.server.ZooKeeperServerMain
import org.apache.zookeeper.server.quorum.QuorumPeerConfig
import kafka.utils.Logging

import java.util.concurrent.Executors
import scala.util.Try
import org.apache.zookeeper.server.ZooKeeperServer
import java.io.File
import org.apache.zookeeper.server.NIOServerCnxn
import java.net.InetSocketAddress
import org.apache.zookeeper.server.NIOServerCnxnFactory
import com.scout24.monitoring.test.ConsumerGroupChecker.ConsumerGroupTestImp
import org.apache.log4j.Logger
import org.apache.log4j.Level

class KafkaLocal(kafkaProperties: Properties, zkProperties: Properties) {
  val logger: Logger = Logger.getLogger(classOf[KafkaLocal].getName)
  logger.setLevel(Level.INFO)

  var kafka: KafkaServerStartable = null
  var zookeeper: ZooKeeperLocal = null

  var kafkaConfig = new KafkaConfig(kafkaProperties)

  //start local zookeeper
  logger.info("starting local zookeeper...")
  zookeeper = new ZooKeeperLocal(zkProperties);
  logger.info("done")

  //start local kafka broker
  kafka = KafkaServerStartable.fromProps(kafkaProperties)
  logger.info("starting local kafka broker...")
  kafka.startup()
  logger.info("done")

  def stop() {
    //stop kafka broker
    logger.info("stopping kafka + zookeeper...")
    kafka.shutdown()
    zookeeper.stop()
    logger.info("done");

  }

}

class ZooKeeperLocal(zkProperties: Properties) extends Runnable {
  val logger: Logger = Logger.getLogger(classOf[ZooKeeperLocal].getName)
  logger.setLevel(Level.INFO)

  var zooKeeperServer: ZooKeeperServerMain = null
  var executors = Executors.newFixedThreadPool(3)

  var clientPort = 2181
  var numConnections = 5000
  var tickTime = 2000

  //var dir: File = new File(ConsumerGroupTestImp.zkPath).getAbsoluteFile();

  //var server = new ZooKeeperServer(dir, dir, tickTime)
  var standaloneServerFactory = new NIOServerCnxnFactory()

  standaloneServerFactory.configure(new InetSocketAddress(clientPort), numConnections)


  executors.submit(this)

  override def run() {
    try {
      //standaloneServerFactory.startup(server);
    } catch {
      case e: Throwable =>
        throw new RuntimeException(e);
    }
  }

  def stop() {
    standaloneServerFactory.shutdown()
  }

}
