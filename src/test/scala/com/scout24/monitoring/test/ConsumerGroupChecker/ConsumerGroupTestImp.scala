package com.scout24.monitoring.test.ConsumerGroupChecker

import org.junit._
import org.scalatest._
import Assert._

import java.util.Properties
import java.util.concurrent.Executors

import scala.collection.mutable.ListBuffer
import org.junit.Test
import org.apache.kafka.common.security.JaasUtils

import kafka.admin.AdminUtils;
import kafka.server.KafkaConfig
import kafka.utils.TestUtils
import kafka.utils.CoreUtils
import kafka.zk.ZooKeeperTestHarness
import kafka.server.KafkaServer

import com.scout24.monitoring.kafka.ConsumerGroupChecker.ConsumerGroupChecker.ConsumerGroupCommandOptions
import com.scout24.monitoring.kafka.ConsumerGroupChecker.ConsumerGroupChecker.KafkaConsumerGroupService
import com.scout24.monitoring.kafka.ConsumerGroupChecker
import com.scout24.runtime.example.ConsumerGroup
import com.scout24.runtime.example.RandomProducer


class ConsumerGroupTestImp extends ZooKeeperTestHarness {
 
  def createKafkaServer(): (String, KafkaServer) = {
    // Create and start broker
    val brokerId1 = 0
    val props1 = TestUtils.createBrokerConfig(brokerId1, zkConnect)
    val server1 = TestUtils.createServer(KafkaConfig.fromProps(props1))
    val port = TestUtils.boundPort(server1)
    val connectionUrl = "localhost:%d".format(port)
    (connectionUrl, server1)
  }
  
  //1. Assume the cluster has a consumer group g1 and a topic t1 with two partitions. 
  //   Offsets for g1 and both partitions t1-0 and t1-1 are stored in Kafka, so no 
  //   missing offsets should be reported.
  @Test
  def testOne() { 
    val topic = "t1"
    val partitions = 2
    val consumerGroupname = "g1"
    val numConsumers = 1
    var numConsumersFailing = 0

    logger.info("Starting testOne")

    // Create and get KafkaServer connection URL
    val (connectionUrl, server1) = createKafkaServer()
    
    val consumers = new ListBuffer[ConsumerGroup]()
    val executor = Executors.newFixedThreadPool(10)
    val topics = List(topic)
    
    // Create Topic direct on Zookeeper
    AdminUtils.createTopic(zkUtils, topic, partitions, 1)

    // Produce some Random messages on the topic
    val producer1: RandomProducer = new RandomProducer(connectionUrl, topic, 10)
    executor.submit(producer1)

    // Start Necessary Consumers and pause some of them if necessary
    for (i <- 0 until numConsumers) {
      var consumer: ConsumerGroup = new ConsumerGroup(connectionUrl, i, consumerGroupname, topics)
      consumers.append(consumer)
      if (numConsumersFailing > 0) {
        consumer.pause()
        numConsumersFailing -= 1
      }
      executor.submit(consumer)
    }

    // Produce more Random messages just in case of the Consumers already consumed all of the previous generated
    val producer2: RandomProducer = new RandomProducer(connectionUrl, topic, 10)
    executor.submit(producer2)

    // Lag Checker
    val props = new Properties()
    props.put("run-forever", "false")
    props.put("bootstrap-server", connectionUrl)
    props.put("topics", topic)
    val opts = new ConsumerGroupCommandOptions(props)
    val consumerGroupCommand = new KafkaConsumerGroupService(opts)

    // action/test
    TestUtils.waitUntilTrue(() => {
      val groups = consumerGroupCommand.listGroups()
      groups.contains(consumerGroupname)
    }, "Expected a different list group results.")
    
    // action/test
    TestUtils.waitUntilTrue(() => {
      var ret = true
      var err: Int = 0
      val (state, assignments) = consumerGroupCommand.describeGroup(consumerGroupname)
      println("Lagged Consumers: %d expected: %d".format(assignments.get.count(_.group == consumerGroupname), numConsumers))
      assignments.get.foreach { consumerAssignment =>
        if (consumerAssignment.lag.getOrElse(0) != 0) {
          ret = false
          err = err + 1
          logger.error("One LAGGED Consumer: %s Offset behind -> %d, Partition -> %s, Topic -> %s".format(consumerAssignment.group, consumerAssignment.lag.getOrElse(0), consumerAssignment.partition.getOrElse("-"), consumerAssignment.topic.getOrElse("-")))
        }
      }
      numConsumersFailing == err
    }, "Expected a different list of lagged servers.", 12000, 3000)
    
    // action/test
    TestUtils.waitUntilTrue(() => {
      val (_, assignments) = consumerGroupCommand.describeGroup(consumerGroupname)
      println("Consumers: %d expected: %d".format(assignments.get.groupBy(_.group).count(_._1 == consumerGroupname), numConsumers))
      assignments.isDefined &&
        assignments.get.groupBy(_.group).count(_._1 == consumerGroupname) == numConsumers
    }, "Expected rows and a consumer id column in describe group results.", 12000, 3000)

    // cleanup
    consumerGroupCommand.close()

    // Unfreeze some consumers
    for (i <- 0 until numConsumersFailing) {
      consumers(i).pause()
    }
    for (consumer <- consumers) {
      consumer.shutdown()
    }

    executor.shutdown()

    server1.shutdown()
    CoreUtils.delete(server1.config.logDirs)
  }
  
  
  //2. Assume the cluster has a consumer group g1 and a topic t1 with two partitions. 
  //   The consumer group g1 uses manual partition assignment, and consumes from 
  //   partition t1-0 only. In this case, missing offsets for topic partition t1-1 
  //   should be reported.
  @Test
  def testTwo() { 
    val topic = "t1"
    val partitions = 2
    val consumerGroupname = "g1"
    val numConsumers = 2
    var numConsumersFailing = 1

    logger.info("Starting testOne")

    // Create and get KafkaServer connection URL
    val (connectionUrl, server1) = createKafkaServer()
    
    val consumers = new ListBuffer[ConsumerGroup]()
    val executor = Executors.newFixedThreadPool(10)
    val topics = List(topic)
    
    // Create Topic direct on Zookeeper
    AdminUtils.createTopic(zkUtils, topic, partitions, 1)

    // Produce some Random messages on the topic
    val producer1: RandomProducer = new RandomProducer(connectionUrl, topic, 10)
    executor.submit(producer1)

    // Start Necessary Consumers and pause some of them if necessary
    for (i <- 0 until numConsumers) {
      var consumer: ConsumerGroup = new ConsumerGroup(connectionUrl, i, consumerGroupname, topics)
      consumers.append(consumer)
      if (numConsumersFailing > 0) {
        consumer.pause()
        numConsumersFailing -= 1
      }
      executor.submit(consumer)
    }

    // Produce more Random messages just in case of the Consumers already consumed all of the previous generated
    val producer2: RandomProducer = new RandomProducer(connectionUrl, topic, 10)
    executor.submit(producer2)

    // Lag Checker
    val props = new Properties()
    props.put("run-forever", "false")
    props.put("bootstrap-server", connectionUrl)
    props.put("topics", topic)
    val opts = new ConsumerGroupCommandOptions(props)
    val consumerGroupCommand = new KafkaConsumerGroupService(opts)

    // action/test
    TestUtils.waitUntilTrue(() => {
      val groups = consumerGroupCommand.listGroups()
      groups.contains(consumerGroupname)
    }, "Expected a different list group results.")
    
    // action/test
    TestUtils.waitUntilTrue(() => {
      var ret = true
      var err: Int = 0
      val (state, assignments) = consumerGroupCommand.describeGroup(consumerGroupname)
      println("Lagged Consumers: %d expected: %d".format(assignments.get.count(_.group == consumerGroupname), 
          numConsumers))
      assignments.get.foreach { consumerAssignment =>
        if (consumerAssignment.lag.getOrElse(0) != 0) {
          ret = false
          err = err + 1
          logger.error("One LAGGED Consumer: %s Offset behind -> %d, Partition -> %s, Topic -> %s".format(
              consumerAssignment.group, consumerAssignment.lag.getOrElse(0), consumerAssignment.partition.getOrElse("-"), 
              consumerAssignment.topic.getOrElse("-")))
        }
      }
      numConsumersFailing == err
    }, "Expected a different list of lagged servers.", 12000, 3000)
    
    // action/test
    TestUtils.waitUntilTrue(() => {
      val (_, assignments) = consumerGroupCommand.describeGroup(consumerGroupname)
      println(" Consumers: %d expected: %d".format(assignments.get.count(_.group == consumerGroupname), numConsumers))
      assignments.isDefined &&
        assignments.get.count(_.group == consumerGroupname) == numConsumers 
    }, "Expected rows and a consumer id column in describe group results.", 12000, 3000)

    // cleanup
    consumerGroupCommand.close()

    // Unfreeze some consumers
    for (i <- 0 until numConsumersFailing) {
      consumers(i).pause()
    }
    for (consumer <- consumers) {
      consumer.shutdown()
    }

    executor.shutdown()

    server1.shutdown()
    CoreUtils.delete(server1.config.logDirs)
  }

  //3. Assume the cluster has two consumer groups g1 and g2, and topics t1 and t2
  //   with one partition each. g1 has offsets for partition t1-0, g2 has offsets 
  //   for partition t2-0. In this case, no missing offsets for g1 and t2-0 
  //   should be reported (as g1 is consuming from topic t1 only), and neither 
  //   should for g2 and t1-0.
  @Test
  def testThree() { 
    val topic1 = "t1"
    val topic2 = "t2"
    val partitions = 1
    val consumerGroupname1 = "g1"
    val consumerGroupname2 = "g2"
    val numConsumers = 1
    var numConsumersFailing = 0

    logger.info("Starting testOne")

    // Create and get KafkaServer connection URL
    val (connectionUrl, server1) = createKafkaServer()
    
    val consumers = new ListBuffer[ConsumerGroup]()
    val executor = Executors.newFixedThreadPool(10)
    val topics = List(topic1, topic2)
    
    // Create Topic direct on Zookeeper
    AdminUtils.createTopic(zkUtils, topic1, partitions, 1)
    AdminUtils.createTopic(zkUtils, topic2, partitions, 1)
    
    // Produce some Random messages on the topic
    val producer1: RandomProducer = new RandomProducer(connectionUrl, topic1, 10)
    executor.submit(producer1)
    val producer2: RandomProducer = new RandomProducer(connectionUrl, topic2, 10)
    executor.submit(producer2)

    // Start Necessary Consumers and pause some of them if necessary
    for (i <- 0 until numConsumers) {
      var consumer1: ConsumerGroup = new ConsumerGroup(connectionUrl, i, consumerGroupname1, List(topic1))
      consumers.append(consumer1)
      var consumer2: ConsumerGroup = new ConsumerGroup(connectionUrl, i, consumerGroupname2, List(topic2))
      consumers.append(consumer2)
      executor.submit(consumer1)
      executor.submit(consumer2)
    }

    // Produce more Random messages just in case of the Consumers already consumed all of the previous generated
    val producer3: RandomProducer = new RandomProducer(connectionUrl, topic1, 10)
    executor.submit(producer3)
    val producer4: RandomProducer = new RandomProducer(connectionUrl, topic2, 10)
    executor.submit(producer4)

    // Lag Checker
    val props = new Properties()
    props.put("run-forever", "false")
    props.put("bootstrap-server", connectionUrl)
    props.put("topics", topics.mkString(","))
    val opts = new ConsumerGroupCommandOptions(props)
    val consumerGroupCommand = new KafkaConsumerGroupService(opts)

    // action/test
    TestUtils.waitUntilTrue(() => {
      val groups = consumerGroupCommand.listGroups()
      groups.contains(consumerGroupname1) &&
      groups.contains(consumerGroupname2)
    }, "Expected a different list group results.")
    
    // action/test
    TestUtils.waitUntilTrue(() => {
      var ret = true
      var err: Int = 0
      val (state, assignments) = consumerGroupCommand.describeGroup(consumerGroupname1)
      println("Lagged Consumers: %d expected: %d".format(assignments.get.count(_.group == consumerGroupname1),
          numConsumers))
      assignments.get.foreach { consumerAssignment =>
        if (consumerAssignment.lag.getOrElse(0) != 0) {
          ret = false
          err = err + 1
          logger.error("One LAGGED Consumer: %s Offset behind -> %d, Partition -> %s, Topic -> %s".format(
              consumerAssignment.group, consumerAssignment.lag.getOrElse(0), consumerAssignment.partition.getOrElse("-"), 
              consumerAssignment.topic.getOrElse("-")))
        }
      }
      numConsumersFailing == err
    }, "Expected a different list of lagged servers.", 12000, 3000)
    
    // action/test
    TestUtils.waitUntilTrue(() => {
      val (_, assignments) = consumerGroupCommand.describeGroup(consumerGroupname1)
      println(" Consumers: %d expected: %d".format(assignments.get.count(_.group == consumerGroupname1), numConsumers))
      assignments.isDefined &&
        assignments.get.count(_.group == consumerGroupname1) == numConsumers 
    }, "Expected rows and a consumerGroupname1 id column in describe group results.", 12000, 3000)

    // action/test
    TestUtils.waitUntilTrue(() => {
      var ret = true
      var err: Int = 0
      val (state, assignments) = consumerGroupCommand.describeGroup(consumerGroupname2)
      println("Lagged Consumers: %d expected: %d".format(assignments.get.count(_.group == consumerGroupname2),
          numConsumers))
      assignments.get.foreach { consumerAssignment =>
        if (consumerAssignment.lag.getOrElse(0) != 0) {
          ret = false
          err = err + 1
          logger.error("One LAGGED Consumer: %s Offset behind -> %d, Partition -> %s, Topic -> %s".format(
              consumerAssignment.group, consumerAssignment.lag.getOrElse(0), consumerAssignment.partition.getOrElse("-"), 
              consumerAssignment.topic.getOrElse("-")))
        }
      }
      numConsumersFailing == err
    }, "Expected a different list of lagged servers.", 12000, 3000)
    
    // action/test
    TestUtils.waitUntilTrue(() => {
      val (_, assignments) = consumerGroupCommand.describeGroup(consumerGroupname2)
      println(" Consumers: %d expected: %d".format(assignments.get.count(_.group == consumerGroupname2), numConsumers))
      assignments.isDefined &&
        assignments.get.count(_.group == consumerGroupname2) == numConsumers 
    }, "Expected rows and a consumerGroupname2 id column in describe group results.", 12000, 3000)
    
    // cleanup
    consumerGroupCommand.close()

    // Unfreeze some consumers
    for (i <- 0 until numConsumersFailing) {
      consumers(i).pause()
    }
    for (consumer <- consumers) {
      consumer.shutdown()
      Thread.sleep(3000)
    }

    executor.shutdown()

    server1.shutdown()
    CoreUtils.delete(server1.config.logDirs)
  }

  
}
