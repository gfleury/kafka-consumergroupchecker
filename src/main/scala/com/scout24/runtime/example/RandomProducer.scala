package com.scout24.runtime.example

import java.util.{ Date, Properties }

import org.apache.kafka.clients.producer.{ KafkaProducer, ProducerRecord }

import scala.util.Random
import org.apache.kafka.clients.producer.RecordMetadata
import org.apache.log4j.Logger
import org.apache.log4j.Level

class RandomProducer(srvBootStrap: String, topic: String, batchSize: Int) extends Runnable {
  val logger: Logger = Logger.getLogger(classOf[RandomProducer].getName)
  logger.setLevel(Level.INFO)
  
  val rnd = new Random()
  val props = new Properties()
  props.put("bootstrap.servers", srvBootStrap)
  props.put("client.id", "RandomProducer")
  props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
  props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")

  var producer = new KafkaProducer[String, String](props)

  override def run() {
    val t = System.currentTimeMillis()
    for (nEvents <- Range(0, batchSize)) {
      val runtime = new Date().getTime()
      val ip = "192.168.2." + rnd.nextInt(255)
      val msg = runtime + "," + nEvents + ",www.example.com," + ip
      val data = new ProducerRecord[String, String](topic, ip, msg)

      producer.send(data, (m: RecordMetadata, e) => { 
        if (e == null) 
          logger.info("MsgSent -> Partition: %d Topic: %s Offset: %d (%s)".format(m.partition(), m.topic(), m.offset(), m.toString()))
          }
      )
      //producer.send(data)
    }

    logger.info("sent per second: %d".format(+ batchSize * 1000 / (System.currentTimeMillis() - t)))
    producer.close()
  }

}