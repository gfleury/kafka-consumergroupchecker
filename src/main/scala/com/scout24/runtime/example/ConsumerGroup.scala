package com.scout24.runtime.example

import java.util.concurrent._
import java.util.{ Collections, Properties }

import kafka.consumer.KafkaStream
import org.apache.kafka.clients.consumer.{ ConsumerConfig, KafkaConsumer }
import org.apache.log4j.Logger

import scala.collection.JavaConverters._
import java.util.UUID
import org.apache.log4j.Level

object ConsumerGroup {
  def main(args: Array[String]) {
    var consumer: ConsumerGroup = new ConsumerGroup("localhost:9092", 0, "cg2", List("t2"))
    consumer.run()
  }
}

class ConsumerGroup(srvBootStrap: String, id: Int, groupId: String, topics: List[String]) extends Runnable {
  val logger: Logger = Logger.getLogger(classOf[ConsumerGroup].getName)
  logger.setLevel(Level.INFO)

  var props = createConsumerConfig(srvBootStrap, groupId)
  var consumer = new KafkaConsumer[String, String](props)
  var isPaused = false
  var shouldIRun = true

  logger.info("Starting consumer %d on topics %s".format(id, topics.toString()))
  consumer.subscribe(topics.asJavaCollection)

  def shutdown() = {
    shouldIRun = false
    isPaused = false
  }

  def createConsumerConfig(srvBootStrap: String, groupId: String): Properties = {
    val props = new Properties()
    props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, srvBootStrap)
    props.put(ConsumerConfig.GROUP_ID_CONFIG, groupId)
    props.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, "1")
    props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false")
    props.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "500")
    //props.put(ConsumerConfig.MAX_POLL_INTERVAL_MS_CONFIG, "3500")
    props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest")
    props.put(ConsumerConfig.CLIENT_ID_CONFIG, UUID.randomUUID().toString().substring(10))
    props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer")
    props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer")
    props
  }

  def pause() {
    isPaused = !isPaused
  }

  override def run() {
    logger.info("Consumer %s entering loop".format(id))
    while (shouldIRun) {
      //logger.debug("Pooling %s %s".format(id, consumer.subscription().toString()))
      val records = consumer.poll(500)

      for (record <- records.asScala if !isPaused) {
        logger.info("%s:%d: Received message: (%s -> %s) at offset %d and partition %d.".format(groupId, id, record.key(), record.value(), record.offset(), record.partition()))
        consumer.commitSync()
      }

      if (isPaused && shouldIRun) {
        //logger.info("%s - Paused".format(groupId))
        Thread.sleep(5000)
      }

    }
    consumer.wakeup()
    if (consumer != null)
      consumer.close();
    logger.info("Shutting down consumer id: %d".format(id))
  }
}
