package com.scout24.monitoring.kafka.ConsumerGroupChecker

import java.util.Properties
import com.scout24.monitoring.kafka.ConsumerGroupChecker.ConsumerGroupChecker.ConsumerGroupCommandOptions
import java.io.{ InputStream, OutputStream, PrintStream }
import org.apache.log4j.Logger
import com.scout24.monitoring.kafka.ConsumerGroupChecker.ConsumerGroupChecker.KafkaConsumerGroupService
import org.apache.log4j.Level

// Wrapper to call locally
object Main {
  
  def main(args: Array[String]) {
    var main = new Main() 
    main.lambda(null, null)
  }
}
class Main {

  val logger = Logger.getLogger(classOf[Main]);
  logger.setLevel(Level.INFO)

  def lambda(input: InputStream, output: OutputStream): Unit = {
    var props = new Properties()
    props.put("bootstrap-server", System.getenv("bootstrap_server"))
    props.put("run-forever", "false")
    props.put("topics", System.getenv("topics"))

    val opts = new ConsumerGroupCommandOptions(props)

    val consumerGroupService = {
      logger.info("Note: This will only show information about consumers that use the Java consumer API (non-ZooKeeper-based consumers).\n")
      new KafkaConsumerGroupService(opts)
    }

    consumerGroupService.run()
  }
}

