/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.scout24.monitoring.kafka.ConsumerGroupChecker

import java.util.Properties

import kafka.api.{ OffsetFetchRequest, OffsetFetchResponse, OffsetRequest, PartitionOffsetRequestInfo }
import kafka.client.ClientUtils
import kafka.common.{ TopicAndPartition, _ }
import kafka.consumer.SimpleConsumer
import kafka.utils._

import org.apache.kafka.clients.CommonClientConfigs
import org.apache.kafka.clients.consumer.{ ConsumerConfig, KafkaConsumer }
import org.apache.kafka.common.Node
import org.apache.kafka.common.TopicPartition
import org.apache.kafka.common.network.ListenerName
import org.apache.kafka.common.protocol.{ Errors, SecurityProtocol }
import org.apache.kafka.common.security.JaasUtils
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.kafka.common.utils.Utils

import scala.collection.JavaConverters._
import scala.collection.{ Set, mutable }
import java.io.FileInputStream
import kafka.admin.AdminClient
import scala.collection.mutable.HashMap

import org.apache.log4j.Logger
import org.apache.log4j.Level

object ConsumerGroupChecker {
  val logger: Logger = Logger.getLogger("ConsumerGroupChecker")
  logger.setLevel(Level.INFO)

  def main(args: Array[String]) {
    val prop = new Properties()
    var confFile = "config.properties"
    try {
      if (args.length <= 1) {
        if (args.length == 1)
          confFile = args(0)

        prop.load(new FileInputStream(confFile))

      } else if (args.length == 3) {
        prop.put("bootstrap-server", args(0))
        prop.put("topics", args(1))
        prop.put("run-forever", args(2))
      } else
        throw new Exception
    } catch {
      case e: Throwable =>
        println("Use: cmd propertie.file")
        println("or   cmd bootstrap-server topics forever")
        println("or   create config.properties and run without args")
        println("propertie request keys: bootstrap-server, topics, run-forever")
        println("Examples: topics = t1,t2,t3 or t1 ")
        println("          run-forever = true or false")
        System.exit(1)
    }

    val opts = new ConsumerGroupCommandOptions(prop)

    val consumerGroupService = {
      logger.info("Note: This will only show information about consumers that use the Java consumer API (non-ZooKeeper-based consumers).\n")
      new KafkaConsumerGroupService(opts)
    }

    consumerGroupService.run()
  }

  val MISSING_COLUMN_VALUE = "-"

  protected case class PartitionAssignmentState(group: String, coordinator: Option[Node], topic: Option[String],
                                                partition: Option[Int], offset: Option[Long], lag: Option[Long],
                                                consumerId: Option[String], host: Option[String],
                                                clientId: Option[String], logEndOffset: Option[Long])

  class KafkaConsumerGroupService(val opts: ConsumerGroupCommandOptions) {

    def run() {
      try {
        do {
          val groupIds = this.listGroups()
          for (groupId <- groupIds) {
            this.checkAssignment(groupId)
          }
          Thread.sleep(2000)
        } while (opts.runModeOpt) 
      } catch {
        case e: Throwable =>
          logger.error("Executing consumer group command failed due to %s".format(Some(e)))
      } finally {
        this.close()
      }

    }

    private val adminClient = createAdminClient()

    // `consumer` is only needed for `describe`, so we instantiate it lazily
    private var consumer: HashMap[String, KafkaConsumer[String, String]] = new HashMap[String, KafkaConsumer[String, String]]()

    def listGroups(): List[String] = {
      adminClient.listAllConsumerGroupsFlattened().map(_.groupId)
    }

    def checkAssignment(groupId: String): Unit = {
      val (state, assignments) = describeGroup(groupId)

      assignments match {
        case Some(assignments) =>
          state match {
            case Some("Dead") | Some("PreparingRebalance") | Some("AwaitingSync") | Some("Stable") =>
              assignments.foreach { consumerAssignment =>
                if (consumerAssignment.lag.getOrElse(0).asInstanceOf[Long] > 1) {
                  logger.error("%s - GroupId:%s Lagged topic:%s partition:%s ActualOffset:%s LaggedBy:%s (%s/%s/%s)".format(state.get, groupId,
                    consumerAssignment.topic.getOrElse(MISSING_COLUMN_VALUE), consumerAssignment.partition.getOrElse(MISSING_COLUMN_VALUE),
                    consumerAssignment.offset.getOrElse(MISSING_COLUMN_VALUE),
                    consumerAssignment.lag.getOrElse(MISSING_COLUMN_VALUE), consumerAssignment.consumerId.getOrElse(MISSING_COLUMN_VALUE),
                    consumerAssignment.host.getOrElse(MISSING_COLUMN_VALUE), consumerAssignment.clientId.getOrElse(MISSING_COLUMN_VALUE)))
                }
              }
            case Some("Empty") =>
            case other =>
              throw new KafkaException(s"Expected a valid consumer group state, but found '${other.getOrElse("NONE")}'.")
          }
      }

    }

    def describeGroup(group: String): (Option[String], Option[Seq[PartitionAssignmentState]]) = {
      val consumerGroupSummary = adminClient.describeConsumerGroup(group)
      (Some(consumerGroupSummary.state),
        consumerGroupSummary.consumers match {
          case None =>
            None
          case Some(consumers) =>
            var assignedTopicPartitions = Array[TopicPartition]()
            val offsets = adminClient.listGroupOffsets(group)
            val rowsWithConsumer =
              if (offsets.isEmpty)
                List[PartitionAssignmentState]()
              else {
                consumers.sortWith(_.assignment.size > _.assignment.size).flatMap { consumerSummary =>
                  val topicPartitions = consumerSummary.assignment.map(tp => TopicAndPartition(tp.topic, tp.partition))
                  assignedTopicPartitions = assignedTopicPartitions ++ consumerSummary.assignment
                  val partitionOffsets: Map[TopicAndPartition, Option[Long]] = consumerSummary.assignment.map { topicPartition =>
                    new TopicAndPartition(topicPartition) -> offsets.get(topicPartition)
                  }.toMap
                  collectConsumerAssignment(group, Some(consumerGroupSummary.coordinator), topicPartitions,
                    partitionOffsets, Some(s"${consumerSummary.consumerId}"), Some(s"${consumerSummary.host}"),
                    Some(s"${consumerSummary.clientId}"))
                }
              }

            val rowsWithoutConsumer = offsets.filterNot {
              case (topicPartition, offset) => assignedTopicPartitions.contains(topicPartition)
            }.flatMap {
              case (topicPartition, offset) =>
                val topicAndPartition = new TopicAndPartition(topicPartition)
                collectConsumerAssignment(group, Some(consumerGroupSummary.coordinator), Seq(topicAndPartition),
                  Map(topicAndPartition -> Some(offset)), Some(MISSING_COLUMN_VALUE),
                  Some(MISSING_COLUMN_VALUE), Some(MISSING_COLUMN_VALUE))
            }
            //opts.topics.foreach(println(_))    
            //(rowsWithConsumer ++ rowsWithoutConsumer).foreach(row => println(row.topic.get))
            Some((rowsWithConsumer ++ rowsWithoutConsumer).filter(row => opts.topics.contains(row.topic.getOrElse(MISSING_COLUMN_VALUE))))
        })
    }

    protected def collectConsumerAssignment(group: String,
                                            coordinator: Option[Node],
                                            topicPartitions: Seq[TopicAndPartition],
                                            getPartitionOffset: TopicAndPartition => Option[Long],
                                            consumerIdOpt: Option[String],
                                            hostOpt: Option[String],
                                            clientIdOpt: Option[String]): Array[PartitionAssignmentState] = {
      if (topicPartitions.isEmpty)
        Array[PartitionAssignmentState](
          PartitionAssignmentState(group, coordinator, None, None, None, getLag(None, None), consumerIdOpt, hostOpt, clientIdOpt, None))
      else {
        var assignmentRows: Array[PartitionAssignmentState] = Array()
        topicPartitions
          .sortBy(_.partition)
          .foreach { topicPartition =>
            assignmentRows = assignmentRows :+ describePartition(group, coordinator, topicPartition.topic, topicPartition.partition, getPartitionOffset(topicPartition),
              consumerIdOpt, hostOpt, clientIdOpt)
          }
        assignmentRows
      }
    }

    protected def getLag(offset: Option[Long], logEndOffset: Option[Long]): Option[Long] =
      offset.filter(_ != -1).flatMap(offset => logEndOffset.map(_ - offset))

    private def describePartition(group: String,
                                  coordinator: Option[Node],
                                  topic: String,
                                  partition: Int,
                                  offsetOpt: Option[Long],
                                  consumerIdOpt: Option[String],
                                  hostOpt: Option[String],
                                  clientIdOpt: Option[String]): PartitionAssignmentState = {
      def getDescribePartitionResult(logEndOffsetOpt: Option[Long]): PartitionAssignmentState =
        PartitionAssignmentState(group, coordinator, Option(topic), Option(partition), offsetOpt,
          getLag(offsetOpt, logEndOffsetOpt), consumerIdOpt, hostOpt,
          clientIdOpt, logEndOffsetOpt)

      getLogEndOffset(group, new TopicPartition(topic, partition)) match {
        case LogEndOffsetResult.LogEndOffset(logEndOffset) => getDescribePartitionResult(Some(logEndOffset))
        case LogEndOffsetResult.Unknown                    => getDescribePartitionResult(None)
        case LogEndOffsetResult.Ignore                     => null
      }
    }

    protected def getLogEndOffset(groupId: String, topicPartition: TopicPartition): LogEndOffsetResult = {
      val consumer = getConsumer(groupId)
      consumer.assign(List(topicPartition).asJava)
      consumer.seekToEnd(List(topicPartition).asJava)
      val logEndOffset = consumer.position(topicPartition)
      LogEndOffsetResult.LogEndOffset(logEndOffset)
    }

    def close() {
      adminClient.close()
      for (c <- consumer.valuesIterator)
        c.close()
    }

    private def createAdminClient(): AdminClient = {
      val props = new Properties()
      props.put(CommonClientConfigs.BOOTSTRAP_SERVERS_CONFIG, opts.bootstrapServerOpt)
      AdminClient.create(props)
    }

    private def getConsumer(groupId: String): KafkaConsumer[String, String] = {
      if (!consumer.contains(groupId))
        consumer.put(groupId, createNewConsumer(groupId))
      consumer.get(groupId).get
    }

    private def createNewConsumer(groupId: String): KafkaConsumer[String, String] = {
      val properties = new Properties()
      val deserializer = (new StringDeserializer).getClass.getName
      properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, opts.bootstrapServerOpt)
      properties.put(ConsumerConfig.GROUP_ID_CONFIG, groupId)
      properties.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false")
      properties.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, "30000")
      properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, deserializer)
      properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, deserializer)

      new KafkaConsumer(properties)
    }

  }

  sealed trait LogEndOffsetResult

  object LogEndOffsetResult {
    case class LogEndOffset(value: Long) extends LogEndOffsetResult
    case object Unknown extends LogEndOffsetResult
    case object Ignore extends LogEndOffsetResult
  }

  class ConsumerGroupCommandOptions(props: Properties) {
    var bootstrapServerOpt: String = "localhost:9092"
    var runModeOpt: Boolean = false
    var topics: Array[String] = Array()
    try {
      bootstrapServerOpt = props.getProperty("bootstrap-server")
      runModeOpt = if (props.getProperty("run-forever").contains("true")) true else false
      topics = props.getProperty("topics").split(",")
    } catch {
      case e: Throwable =>
        logger.error("Properties files necessary with bootstrap-server, run-forever and topics")
    }
  }
}